---
title: JS Patterns
---

# Posit Function
Good for keeping business logic organised and easily handling error cases in a server controller.

```javascript
const posit = await positRun({ input, actions, cases });
// posit = { perfect: true, error: false, output: {} }
```

Mostly business logic is about taking a set of values and either transforming them or using them to create new values, or a mix of both.

You want **input** values and **actions** to take on those values. Here's the simplest case:

```javascript
const { perfect, error, output } = await positRun({
  input: {
    orderId: '123',
    order: null,
  },
  actions: {
    order: (input) => Order.findById(input.orderId),
  },
});

if (perfect && !error) {
    return output;
    // output = { orderId: '123', order: OrderObject }
}
```

You might also want condition **cases** to validate those values...

```javascript
const { perfect, error, output } = await positRun({
  input: {
    orderId: '123',
    order: null,
  },
  actions: {
    order: (input) => Order.findById(input.orderId),
  },
  cases: {
    order: (input) => {
        if (!input.order) {
            return { message: 'No order found' };
        }
    }
  },
});

// If the case condition returns anything, posit will set "perfect" to false,
// and set the "output" to whatever you returned...

if (!perfect) {
    return output.message;
    // output.message = 'No order found'
}

if (perfect && !error) {
    return output;
    // output = { orderId: '123', order: OrderObject }
}
```

## Actions
Actions run in the order they're declared. They also recieve the values from the previous action.

```javascript
const { output } = await positRun({
  input: {
    one: null,
    two: null,
    three: 'something',
  },
  actions: {
    // Sets one to 'something'
    one: ({ one two three }) => three,
    // Passes one: 'something' as a parameter, set's two to 'something'
    two: ({ one two three }) => one,
    // Sets three to null
    three: ({ one two three }) => null,
  },
});

// output = { one: 'something', two: 'something', three: null }
```

## Cases
Cases break the chain of actions, if a case returns something it will prevent the later actions from running.
A case runs **after** the action with the same name.

```javascript
const { output } = await positRun({
  input: {
    one: null,
    two: null,
    three: 'something',
  },
  actions: {
    one: ({ one two three }) => three,
    two: ({ one two three }) => one,
    three: ({ one two three }) => null,
  },
  cases: {
    one: ({ one, two, three }) => {
        if (one === 'something') {
            return { message: 'Boom' };
        }
    },
  },
});

// actions two and three never ran...
// output = { one: 'something', two: null, three: 'something' }
```

## Errors
Cases are designed to check for errors but they don't catch errors. If an error's thrown `positRun` won't break, instead it'll set `error` to true and set the `output` to an error object.

```javascript
const { perfect, error, output } = await positRun({
  input: {
    // Giving Order.findById a null id will cause it to throw an error...
    orderId: null,
  },
  actions: {
    order: (input) => Order.findById(input.orderId),
  },
});

// error will be true and output becomes an Error instance...
if (error) {
    return output;
}
```

You can catch the error easily by adding a catch to the query and returning a value to the case function...

```javascript
const { perfect, error, output } = await positRun({
  input: {
    // Giving Order.findById a null id will cause it to throw an error...
    orderId: null,
  },
  actions: {
    order: (input) =>
      Order.findById(input.orderId).catch(() => 'Bad request'),
  },
  cases: {
    order: ({ order }) =>
      order === 'Bad request' && { message: 'The Order query broke' },
  },
});

// This time error will be false because we handled the error ourselves...
if (error) {
    return output;
}

// But perfect will still be false caused by the case function...
if (!perfect) {
    return output;
    // output = { message: 'The Order query broke' }
}
```